:caption: Contents:

intro
user_guide
api_reference

API Reference
==============

General
-----------------------
.. automodule:: loggol
   :members:
   :show-inheritance:

Formatter
-----------------------

.. automodule:: loggol.formatter
   :members:
   :show-inheritance:

Handlers
----------------------

.. automodule:: loggol.handlers
   :members:
   :show-inheritance:

Logger
--------------------

.. automodule:: loggol.logger
   :members:
   :show-inheritance:

Outputs
--------------------

.. automodule:: loggol.output
   :members:
   :show-inheritance:
