import loggol

sampl_dict = {
    'a': 1,
    'b': 2,
    'c': 3
}


def sample_func(attr):
    try:
        return sampl_dict[attr]
    except KeyError:
	logger.exception('Missing attribute: {}'.format(attr))
  
	# OR use:
	# logger.error('Missing attribute: {}'.format(attr), exc_info=True)

        return None


if __name__ == "__main__":
    loggol.setup(name='traceback_log')

    logger = loggol.get_logger('traceback_log')
    logger.info('Some sample info message')
    smpl_ret_val = sample_func('d')



