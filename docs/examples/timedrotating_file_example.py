import loggol

loggol.setup(
    name='timerot_example',
    level='DEBUG',
    outputs=[
        loggol.output.TimedRotatingFile(
            filename='test_timedfilerot_log',
            interval=2, when='H')]) # rotate every 2 hours

"""
Also timedelta can be passed instead integer like:

    loggol.output.TimedRotatingFile(filename='test_file_log', interval=datetime.timedelta(hours=2))
"""


logger = loggol.get_logger('timerot_example')

logger.info('Test info message')
logger.error('Test error message')