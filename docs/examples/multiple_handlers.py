import loggol

loggol.setup(
    name='multi_handlers_test',
    outputs=[
        loggol.output.Stream(),
        loggol.output.File(
            filename="multi_handlers_test",
            directory='<PATH TO DIR>')])

logger = loggol.get_logger('multi_handlers_test')

logger.info('Test info message')
logger.error('Test error message')
