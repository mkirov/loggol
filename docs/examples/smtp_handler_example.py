import loggol

if __name__ == "__main__":

    loggol.setup(
        name='test_log',
        outputs=[
            loggol.output.Stream(
                level=loggol.levels.INFO
            ),
            loggol.output.SMTP(
                mailhost='localhost',
                fromaddr='sender@test.com',
                toaddrs=['reciever@test.com'],
                subject='this is a test from logger',
                level=loggol.levels.ERROR)]) # send mail only on Errors

    logger = loggol.get_logger('test_log')

    logger.info('some info message')
    logger.error('some error message')
