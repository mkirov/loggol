import loggol


# specify logger attribute name within class usage (default is "logger")

@loggol.class_logger(name='class_log_test', attr_name='test_logger')
class TestClass:

    def __init__(self, a):
        self.a = a
        self.test_logger.info('Checking Init ...')

    def test_func(self, a):

        c = a + 5
        self.test_logger.debug(f'Function returns {c}')

        return c


@loggol.class_logger(name='class_log_test')
class AnotherTestClass:

    def show_full_name(self):

        self.logger.debug(f'Showing full name ... ')
        full_name = ' '.join((self.first_name, self.last_name))

        return full_name

    def show_full_name_reverse(self):
        self.logger.warning('Warning in this function')

        full_name = ' '.join((self.last_name, self.first_name))

        return full_name


class YetAnotherTestClass(AnotherTestClass):
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.logger.warning('another Warning in this function')
        self.logger.info(f'First name passed in init is: {self.first_name}')


class ExampleClass:
    """
        If you want to manually add logger to your class instead of using the decorator
    """

    def __init__(self):
        self.logger = loggol.ClassLogWrapper('class_log_test', self.__class__)
        self.logger.info('Init from example class ...')


if __name__ == "__main__":

    loggol.setup(
        'class_log_test',
        outputs=[
            loggol.output.Stream(
                level=loggol.Levels.DEBUG,
                format="{name} - {trace_path} - {Font=BLACK}{msg}{Style=CLEAR}"),
            loggol.output.File(filename='class_log', level=loggol.Levels.WARNING)])

    t = TestClass(12)
    t.test_func(5)

    t2 = YetAnotherTestClass('John', 'Wick')
    t2.show_full_name()
    t2.show_full_name_reverse()

    t3 = ExampleClass()
