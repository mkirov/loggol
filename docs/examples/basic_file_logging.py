import loggol

loggol.setup(
    name='console_test',
    outputs=[
        loggol.output.File(
            filename='test_file',
            directory='example/directory')])

logger = loggol.get_logger('console_test')

logger.info('Test info message')
logger.error('Test error message')
