from logging.handlers import QueueListener


__all__ = ["QListener"]


class QListener(QueueListener):

    def handle(self, record):
        """ Override handle a record.

            This just loops through the handlers
            offering them the record to handle.

            :param record: The record to handle.
        """

        record = self.prepare(record)
        for handler in self.handlers:
            if record.levelno >= handler.level:
                handler.handle(record)
