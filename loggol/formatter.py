import os
import json
from copy import copy
from datetime import datetime

from logging import Formatter

from loggol.constants import Common
from loggol.string_formatter import LoggolStringFormatter
from loggol.record_serializer import RecordSerializer


__all__ = ["LoggolFormatter"]


class LoggolFormatter(Formatter):
    """ The default formatter that is used in all outputs.

        :type kwargs: dict
        :key format: format string of the message. Default is `constants.Common.DEFAULT_FORMAT`
    """

    def __init__(self, format=None, extras=None, serialize=False, as_dict=False):

        self._format = Common.DEFAULT_FORMAT
        self._extras = dict()
        self._serialize = serialize
        self._as_dict = as_dict

        if self._serialize and self._as_dict:
            raise RuntimeError(
                'Record format cannot be as dict and json simultaneously.'
                'Choose either one of them.')

        if format is not None:
            self._format = format

        if extras is not None:
            self._extras = extras

        super().__init__()

    def format(self, record):
        """ This method doesn't work
            directly on the LogRecord object, but copies it
            to avoid problems when modifying it directly.
            Extracts class names from
            logger name using class separator.
            Uses :class:`LoggolStringFormatter`
            which overrides :class:`string.StringFormatter` to
            format the message with a special string format
            containing styles and chosen record attributes.

            :param record: logger record to format
            :type record: :class:`LogRecord`
        """

        curr_time = datetime.now()

        record = copy(record)

        record_frmt = self._format

        if hasattr(record, 'new_msg'):
            return record.new_msg

        is_tty = getattr(record, 'stream_is_a_tty', False)
        colorized = getattr(record, 'colorized', False)

        record.exception = getattr(record, 'exception', None)
        record.class_name = None
        record.function_name = None
        record.module_name = None
        record.class_trace = None
        record.trace_path = None
        record.module_name = os.path.splitext(record.filename)[0]

        if record.exc_info:
            record.exception = self.formatException(record.exc_info)

        if record.funcName \
           and not record.funcName.startswith('<') \
           and not record.funcName.endswith('>'):

            record.function_name = ''.join((str(record.funcName), '()'))

        split_record_name = record.name.split(Common.CLASS_SEPARATOR)
        if len(split_record_name) > 1:
            record.name = split_record_name[0].replace(".", '')
            record.class_trace = '.'.join(split_record_name[1:])

        record.trace_path = '.'.join(
            filter(None,
                   [getattr(record, 'module_name', ''),
                    getattr(record, 'class_trace', '')]))

        record_result = dict(
            time=curr_time,
            name=record.name,
            lineno=record.lineno,
            msg=record.msg,
            filename=record.filename,
            module_name=record.module_name,
            level=record.levelname,
            levelno=record.levelno,
            thread=record.thread,
            thread_name=record.threadName,
            process=record.process,
            process_name=record.processName,
            function_name=record.function_name,
            class_trace=record.class_trace,
            trace_path=record.trace_path,
            pathname=record.pathname,
            exception=record.exception
        )

        for extra in self._extras:
            if extra not in record_result:
                record_result[extra] = self._extras[extra]

        if self._serialize:
            return json.dumps(record_result, indent=True, cls=RecordSerializer)
        elif self._as_dict:
            return record_result
        else:
            if record_result.get('exception'):
                record_frmt = os.linesep.join((record_frmt, '{exception}'))

            value_formatter = LoggolStringFormatter(is_tty, colorized)
            return value_formatter.format(record_frmt, **record_result)
