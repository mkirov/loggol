
import os
import sys
import inspect
from queue import Queue

from logging import FileHandler

from logging.handlers import SMTPHandler
from logging.handlers import QueueListener
from logging.handlers import RotatingFileHandler

from loggol.formatter import LoggolFormatter

from loggol.handlers import CronTimedFileHandler
from loggol.handlers import RainbowStreamHandler
from loggol.handlers import QHandler
from loggol.handlers import SQSHandler
from loggol.handlers import ConcurrentRotatingFileHandler
from loggol.handlers import HTTPHandler

from loggol.exceptions import LogPathError
from loggol.exceptions import LogOutputError

from loggol import size_parser


class BaseOutput:

    def __init__(self, handler, level=None,
                 format=None, extras=None,
                 serialize=None):

        self.handler = handler

        formatter = LoggolFormatter(format, extras, serialize)
        self.handler.setFormatter(formatter)

        if level is not None:
            self.handler.setLevel(level)

    def add_to_logger(self, logger):

        logger.addHandler(self.handler)


class BaseFileOutput(BaseOutput):

    def _get_log_file_path(self, logfile=None,
                           log_dir=None, suffix=None):

        ret_path = None

        if suffix is None and not os.path.splitext(logfile)[1]:
            logfile = '.'.join((logfile, 'log'))

        if not log_dir:
            ret_path = logfile

        if ret_path is None and logfile and log_dir:
            ret_path = os.path.join(log_dir, logfile)

        if ret_path is None and log_dir:
            program_name = self._get_program_name()
            ret_path = os.path.join(log_dir, program_name, suffix)

        if ret_path is None:
            raise LogPathError('Unable to determine log file destination')

        return ret_path

    @staticmethod
    def _get_program_name():

        stack = inspect.stack()
        return os.path.basename(
            stack[-1][1]).split('.')[0]


class BasicFileOutput(BaseFileOutput):
    """
        Basic file output

        :param filename: Name of the log file
        :type filename: str
        :param directory: Directory of the log file
        :type directory: str
        :param suffix: Suffix for the log file
        :type suffix: str
        :param delay: Delays log file creation until
            the first call.
        :type delay: bool
        :param overwrite: Overwrites the file instead of appending to id
        :type overwrite: bool
        :param level: Level of the output
        :type level: str
         :param format: Format of the handler
        :type format: str
        :param extras: Extras for the handler
        :type extras: dict
        :param serialize: Turn on/off log messages JSON serializing.
            Default is False
        :type serialize: bool
    """

    def __init__(self, filename=None, directory=None,
                 suffix=None, overwrite=False, level=None, encoding=None,
                 delay=True, format=None, extras=None,
                 serialize=None):

        log_path = self._get_log_file_path(
            filename, directory, suffix)

        if overwrite:
            file_mode = 'w'
        else:
            file_mode = 'a'

        handler = FileHandler(log_path, mode=file_mode, encoding=encoding, delay=delay)

        super().__init__(handler, level, format, extras, serialize)


class SizeRotatingFileOutput(BaseFileOutput):
    """
        Rotating file output
        This output is multiprocessing safe, hence
        you can write to a single file with multiple processes.
        It uses lock file with name of a filename basename to coordinate
        the rotations.

        :param filename: Name of the log file
        :type filename: str
        :param directory: Directory for the log file
        :type directory: str
        :param suffix: Suffix for the log file
        :type suffix: str
        :param level: Level of the output
        :type level: str
        :param delay: Delays log file creation until
            the first call.
        :type delay: bool
        :param size: Max log file size
            before start rotating
        :type size: int
        :param backup: Count of files
            kept as backup after rotation
        :type backup: int
        :param format: Format of the handler
        :type format: str
        :param extras: Extras for the handler
        :type extras: dict
        :param serialize: Turn on/off log messages JSON serializing.
            Default is False
        :type serialize: bool

    """

    def __init__(self, filename=None,
                 directory=None, suffix=None,
                 level=None, size=0,
                 delay=True, concurrent=False,
                 backup=0, format=None,
                 extras=None, serialize=None):

        log_path = self._get_log_file_path(
            filename, directory, suffix)

        log_size_limit = size_parser.parse_human_readable_size(size)

        if concurrent:
            file_handler = ConcurrentRotatingFileHandler
        else:
            file_handler = RotatingFileHandler

        handler = file_handler(
            log_path,
            maxBytes=log_size_limit,
            backupCount=backup,
            delay=delay)

        super().__init__(handler, level, format, extras, serialize)


class CronRotatingFileOutput(BaseFileOutput):
    """
        Time rotating file output.
        This output is not multiprocessing safe.
        Uses cron string to define the interval between file rotations

        :param filename: Name of the log file
        :type filename: str
        :param directory: Directory for the log file
        :type directory: str
        :param suffix: Suffix for the log file
        :type suffix: str
        :param level: Level of the output
        :type level: str
        :param delay: Delays log file creation until
            the first call.
        :type delay: bool
        :param backup: Count of files
            kept as backup after rotation
        :type backup: int
         :param format: Format of the handler
        :type format: str
        :param extras: Extras for the handler
        :type extras: dict
        :param serialize: Turn on/off log messages JSON serializing.
            Default is False
        :type serialize: bool
    """

    def __init__(self, filename=None, cron=None,
                 directory=None, suffix=None, level=None,
                 backup=0, delay=True, compressed=False,
                 utc=False, format=None, extras=None, serialize=None):

        log_path = self._get_log_file_path(
            filename, directory, suffix)

        handler = CronTimedFileHandler(
            log_path,
            backupCount=backup,
            delay=delay,
            cron=cron,
            utc=utc,
            compressed=compressed)

        super().__init__(handler, level, format, extras, serialize)


class EmailOutput(BaseOutput):
    """
        Email Output for sending mails

        :param mail_host: SMTP host, can use tuple
            to specify host (host, port)
        :type mail_host: tuple/str
        :param from_address: Address of the sender
        :type from_address: str
        :param to_address: Address/es of the reciever/s
        :type to_address: list
        :param subject: Subject of the mail
        :type subject: str
        :param credentials: Credentials for the SMTP server
            in tuple format (username, password)
        :type credentials: tuple
        :param secure: Specifies use of secure protocol
            Only works with credentials
        :type secure: tuple
        :param timeout: specifies timeout for communication
            with the SMTP server
        :param level: Level of the output
        :type level: str
        :param format: Format of the handler
        :type format: str
        :param extras: Extras for the handler
        :type extras: dict
    """

    def __init__(self, mail_host, from_address,
                 to_address, subject, credentials=None,
                 secure=None, timeout=1.0, level=None,
                 format=None, extras=None):

        handler = SMTPHandler(
            mail_host, from_address,
            to_address, subject,
            credentials, secure, timeout)

        super().__init__(handler, level, format, extras)


class HTTPOutput(BaseOutput):
    """
        HTTP output

        :param url: Url of the service that logs will be sent to
        :type url: str
        :param auth: Authorization user and password if needed
        :type auth: tuple
        :param verify: Verify the host
        :type verify: bool
        :param level: Level of the output
        :type level: str
        :param format: Format of the output
        :type format: str
        :param extras: Extas of the output
        :type extras: dict

    """

    def __init__(self, url, auth=None,
                 verify=None, level=None,
                 format=None, extras=None):

        handler = HTTPHandler(url, auth, verify)

        super().__init__(handler, level, format, extras, serialize=True)


class StreamOutput(BaseOutput):
    """
        Stream output

        :param stream: Stream type. Default is "stderr".
        :param level: Level of the output
        :type level: str
        :param colorized: Turn on/off log messages colorizing.
            Default is True
        :type colorized: bool
        :param format: Format of the handler
        :type format: str
        :param extras: Extras for the handler
        :type extras: dict
        :param serialize: Turn on/off log messages JSON serializing.
            Default is False
        :type serialize: bool

    """

    def __init__(self, stream=sys.stderr,
                 level=None, colorized=True,
                 format=None, extras=None, serialize=False):

        handler = RainbowStreamHandler(stream, colorized)

        super().__init__(handler, level, format, extras, serialize)


class SQSOutput(BaseOutput):
    """
        SQS output.
        Can be used to send log messages to a sqs queue.
        Messages will be JSON serialized.

        :param queue: queue name
        :type queue: str
        :param aws_key_id: Stream type
        :type aws_key_id: str
        :param secret_key: Stream type
        :type secret_key: str
        :param aws_region: Stream type
        :type aws_region: str
        :param level:Level of the output
        :type level: str
        :param extras: Extras of the output
        :type extras: dict
        :param format: Format of the output
        :type format: str

    """

    def __init__(
            self, queue, aws_key_id=None,
            secret_key=None, aws_region=None,
            format=None, level=None, extras=None):

        handler = SQSHandler(
            queue,
            aws_key_id=aws_key_id,
            secret_key=secret_key,
            aws_region=aws_region)

        super().__init__(
            handler,
            level=level,
            format=format,
            extras=extras,
            serialize=True)


class QueueOutput(BaseOutput):
    """
        Queue output supports sending
        messages to a queue.
        Can be used to let outputs do their
        work on a separate thread from the one
        which does the logging so it don't slow the
        main process (for example sending log errors
        to a mail)

        :param outs: List of outputs which will emit
            to the queue
        :type outs: list

    """

    def __init__(self, outs):

        q = Queue(-1)

        queue_handlers = list()
        for fh in outs:
            if isinstance(fh, BaseOutput):
                queue_handlers.append(fh.handler)
            else:
                queue_handlers.append(fh)

        if not queue_handlers:
            raise LogOutputError(
                'No outputs passed to the queue listener')

        self.ql = QueueListener(q, *queue_handlers)
        self.ql.start()

        qh = QHandler(q)

        super().__init__(qh)

    def stop_queue_listener(self):
        self.ql.stop()
