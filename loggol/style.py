
__all__ = ["Font", "Highlight", "Style"]


class StyleConverter:

    CSI = '\x1b['

    def __init__(self):
        for k, v in self._ITEMS.items():
            setattr(
                self, k,
                ''.join((self.CSI, str(v), 'm')))


class HighlightStyle(StyleConverter):

    _ITEMS = {
        'BLACK': 40,
        'RED': 41,
        'GREEN': 42,
        'YELLOW': 43,
        'BLUE': 44,
        'MAGENTA': 45,
        'CYAN': 46,
        'WHITE': 47
    }


class FontStyle(StyleConverter):

    _ITEMS = {
        'BLACK': 30,
        'RED': 31,
        'GREEN': 32,
        'YELLOW': 33,
        'BLUE': 34,
        'MAGENTA': 35,
        'CYAN': 36,
        'WHITE': 37
    }


class BaseStyle(StyleConverter):

    _ITEMS = {
        'CLEAR': 0,
        'BOLD': 1,
        'UNDERLINE': 4
    }


Font = FontStyle()
Highlight = HighlightStyle()
Style = BaseStyle()
