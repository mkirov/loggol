class LoggerError(Exception):
    pass


class LogPathError(Exception):
    pass


class LevelError(Exception):
    pass


class LogOutputError(Exception):
    pass
