import inspect
from logging import Logger

from loggol.output import QueueOutput


class Logger(Logger):
    """ Overrides :class:`logging.Logger` so all
        methods from it can also be used.

        :param name: logger name
        :type name: str

    """

    def __init__(self, name):
        self._outputs = list()
        self._default_output = None
        super().__init__(name)

    @property
    def outputs(self):
        return self._outputs

    @outputs.setter
    def outputs(self, value):
        self._outputs.append(value)

    @property
    def default_output(self):
        return self._default_output

    @default_output.setter
    def default_output(self, value):
        if self._default_output is None:
            self._default_output = value
            self.add_output(value)

    def get_outputs(self, out_type):
        found_outs = list()
        if inspect.isclass(out_type):
            raise RuntimeError(
                '"get_outputs" expects a class not an instance')

        for output in self.outputs:
            if isinstance(output, out_type):
                found_outs.append(output)

        return found_outs

    def add_outputs(self, outs):
        """ Adds multiple outputs from list to the logger

            :param outs: List of outputs to
                add to the logger
            :type outs: list
            :return: list of instances of the added outputs
            :rtype: list
        """

        added_outputs = list()
        for out in outs:
            added_outputs.append(
                self.add_output(out))

        return added_outputs

    def add_output(self, out):
        """ Adds single output to the logger

            :param out: Output to be added to the logger
            :return: instance of the added output
        """

        if self.default_output in self.outputs:
            self.remove_output(self.default_output)

        self.outputs = out
        out.add_to_logger(self)

        return out

    def remove_outputs(self, outs: list):
        """ Removes multiple outputs from a list

            :param outs: List of outputs to be removed
                from the logger.
            :type outs: list
        """

        outs = outs.copy()

        for out in outs:
            self.remove_output(out)

    def remove_output(self, out):
        """ Removes single output from the logger

            :param out: Output to be removed from the logger
        """

        if isinstance(out, QueueOutput):
            out.stop_queue_listener()

        if out not in self.outputs:
            raise RuntimeError(
                'Output ({}) trying to remove does not '
                'exist in logger output list. '
                'Probably is already deleted'.format(out))

        output_handler = out.handler
        output_handler.close()

        self.removeHandler(output_handler)
        self.outputs.remove(out)
