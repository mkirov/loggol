import os
import re
import zipfile
import copy
import boto3
import requests
import base64

from croniter import croniter
from stat import ST_MTIME
from datetime import datetime
from portalocker import LOCK_EX, lock, unlock

from logging import FileHandler
from logging import StreamHandler
from logging import Handler

from logging.handlers import QueueHandler
from logging.handlers import BaseRotatingHandler


__all__ = [
    'RainbowStreamHandler', 'QHandler',
    'ConcurrentRotatingFileHandler', 'CronTimedFileHandler',
    'HTTPHandler', 'SQSHandler']


class RainbowStreamHandler(StreamHandler):

    def __init__(self, stream, colorized):

        self.colorized = colorized
        super().__init__(stream)

    def format(self, record):

        if hasattr(self.stream, 'isatty'):
            record.stream_is_a_tty = self.stream.isatty()
        else:
            record.stream_is_a_tty = False

        record.colorized = self.colorized

        stream_format = super().format(record)
        del record.stream_is_a_tty

        return stream_format


class LockBaseRotatingHandler(BaseRotatingHandler):

    def __init__(self, filename, mode='a', encoding=None, delay=True):

        super().__init__(filename, mode=mode, encoding=encoding, delay=delay)

        self.stream_lock = None
        self.lock_file_name = self.get_lock_filename()
        self.is_locked = False

    def get_lock_filename(self):

        lock_file = os.path.splitext(self.baseFilename)[0]

        lock_file = '.'.join((str(lock_file), 'lock'))
        lock_path, lock_name = os.path.split(lock_file)

        lock_name = ''.join((".__", str(lock_name)))
        return os.path.join(lock_path, lock_name)

    def _open_lockfile(self):
        if self.stream_lock and not self.stream_lock.closed:
            return

        lock_file = self.lock_file_name

        self.stream_lock = open(lock_file, "wb", buffering=0)

    def emit(self, record):
        try:
            try:
                self._do_lock()
                try:
                    if self.shouldRollover(record):
                        self.doRollover()

                    FileHandler.emit(self, record)
                except Exception:
                    self.handleError(record)
            finally:
                self._do_unlock()
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception:
            self.handleError(record)

    def _do_lock(self):
        if self.is_locked:
            return

        self._open_lockfile()

        if self.stream_lock:
            for i in range(5):
                try:
                    lock(self.stream_lock, LOCK_EX)
                    self.is_locked = True
                    break
                except Exception:
                    continue
            else:
                raise RuntimeError("Cannot acquire lock")

    def _do_unlock(self):

        if self.stream_lock:
            if self.is_locked:
                unlock(self.stream_lock)
                self.is_locked = False

            self.stream_lock.close()
            self.stream_lock = None

    def _close(self):

        if self.stream:
            try:
                if not self.stream.closed:
                    self.stream.flush()
                    self.stream.close()
            finally:
                self.stream = None


class ConcurrentRotatingFileHandler(LockBaseRotatingHandler):

    def __init__(self, filename, mode='a', maxBytes=0, backupCount=0, delay=True):

        self.maxBytes = maxBytes
        self.backupCount = backupCount

        super().__init__(filename, mode, delay=delay)

    def shouldRollover(self, record):
        if self.maxBytes > 0:
            self.stream = self._open()
            try:
                self.stream.seek(0, 2)
                if self.stream.tell() >= self.maxBytes:
                    return True
            finally:
                self._close()
        return False

    def doRollover(self):

        if self.stream:
            self.stream.close()
            self.stream = None

        if self.backupCount > 0:
            for i in range(self.backupCount - 1, 0, -1):
                sfn = self.rotation_filename("{}.{}".format(self.baseFilename, i))
                dfn = self.rotation_filename("{}.{}".format(self.baseFilename, i + 1))
                if os.path.exists(sfn):
                    if os.path.exists(dfn):
                        os.remove(dfn)
                    os.rename(sfn, dfn)

            dfn = self.rotation_filename('.'.join((self.baseFilename, "1")))

            if os.path.exists(dfn):
                os.remove(dfn)
            self.rotate(self.baseFilename, dfn)

        if not self.delay:
            self.stream = self._open()


class CronTimedFileHandler(BaseRotatingHandler):

    extMatch = re.compile(r"^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}(\.\w+)?$", re.ASCII)

    def __init__(self, filename, cron=None, backupCount=0,
                 encoding=None, delay=False, utc=False, compressed=False):

        super().__init__(filename, mode='a', encoding=encoding, delay=delay)

        self.backupCount = backupCount
        self.delay = delay
        self.utc = utc
        self.compressed = compressed

        if cron is None or not croniter.is_valid(cron):
            raise ValueError('Invalid or non-defined cron expression')

        self.filename, self.file_extension = os.path.splitext(self.baseFilename)

        start_time = None

        if os.path.exists(filename):
            start_time = datetime.fromtimestamp(
                os.stat(filename)[ST_MTIME])

        if start_time is None:
            if utc:
                start_time = datetime.utcnow()
            else:
                start_time = datetime.now()

        self.cron_iter = croniter(cron, start_time)
        self.rollover_at = self.cron_iter.get_next(datetime)

    def shouldRollover(self, record):

        current_time = datetime.now()

        if current_time >= self.rollover_at:
            return 1

        return 0

    def doRollover(self):

        if self.stream:
            self.stream.close()
            self.stream = None

        if not self.utc:
            current_time = datetime.now()
        else:
            current_time = datetime.utcnow()

        dfn = self.rotation_filename(
            '.'.join(
                [self.filename,
                 datetime.strftime(current_time, '%Y-%m-%d_%H-%M-%S')]))

        if os.path.exists(''.join((dfn, self.file_extension))):
            os.remove(''.join((dfn, self.file_extension)))

        self.rotate(self.baseFilename, ''.join((dfn, self.file_extension)))

        if self.compressed:
            with zipfile.ZipFile(
                    '.'.join((dfn, "zip")),
                    mode='w',
                    compression=zipfile.ZIP_DEFLATED) as zip_hndl:

                zip_hndl.write(
                    ''.join((dfn, self.file_extension)),
                    os.path.basename(''.join((dfn, self.file_extension))))

            os.remove(''.join((dfn, self.file_extension)))

        if self.backupCount > 0:
            for s in self.getFilesToDelete():
                os.remove(s)

        if not self.delay:
            self.stream = self._open()

        new_rollover_at = self.cron_iter.get_next(datetime)

        while new_rollover_at <= current_time:
            new_rollover_at = self.cron_iter.get_next(datetime)

        self.rollover_at = new_rollover_at

    def getFilesToDelete(self):

        dirName, baseName = os.path.split(self.filename)
        fileNames = os.listdir(dirName)
        result = []
        prefix = baseName + "."
        plen = len(prefix)
        for fileName in fileNames:
            if fileName[:plen] == prefix:
                suffix = fileName[plen:]
                if self.extMatch.match(suffix):
                    result.append(os.path.join(dirName, fileName))

        if len(result) < self.backupCount:
            result = []
        else:
            result.sort()
            result = result[:len(result) - self.backupCount]

        return result


class QHandler(QueueHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def prepare(self, record):

        record = copy.copy(record)

        if record.exc_info:
            record.exception = self.formatter.formatException(record.exc_info)

        record.args = None
        record.exc_info = None
        record.exc_name = None

        return record


class SQSHandler(Handler):

    def __init__(self, queue, aws_key_id=None, secret_key=None, aws_region=None):

        super().__init__()

        client = boto3.resource(
            'sqs',
            aws_access_key_id=aws_key_id,
            aws_secret_access_key=secret_key,
            region_name=aws_region)

        self.queue = client.get_queue_by_name(QueueName=queue)

        self._entrance_flag = False

    def emit(self, record):

        if not self._entrance_flag:
            msg = self.format(record)

            self._entrance_flag = True
            try:
                self.queue.send_message(MessageBody=msg)
            finally:
                self._entrance_flag = False


class HTTPHandler(Handler):

    def __init__(self, url, auth=None, verify=True):

        super().__init__()

        self.url = url
        self.auth = auth
        self.verify = verify
        self.headers = dict()

    def emit(self, record):

        data = self.format(record)

        if self.auth:
            if isinstance(self.auth, tuple):
                s = ('%s:%s' % self.auth).encode('utf-8')
                s = 'Basic ' + base64.b64encode(s).strip().decode('ascii')
                self.headers['Authorization'] = s
            elif isinstance(self.auth, str):
                self.headers['Authorization'] = self.auth
            else:
                raise ValueError('Unknown type for "auth" parameter')

        self.headers['Content-Type'] = 'application/json'

        response = requests.post(
            self.url,
            headers=self.headers,
            data=data,
            verify=self.verify)

        if not response:
            raise RuntimeError('Problem with sending data')
