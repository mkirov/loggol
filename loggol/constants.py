
__all__ = ["Common", "Colors", "Styles"]


class Common:
    CLASS_SEPARATOR = '@@@'
    DEFAULT_FORMAT = (
        '{Font=BLACK}{Style=BOLD}[{time:%F %T}]{Style=CLEAR} '
        '{Font=WHITE}{name}{Style=CLEAR} {level} '
        '{Font=GREEN}{trace_path}:{lineno}{Style=CLEAR} '
        '{Style=LEVEL}{msg}{Style=CLEAR}')


class Colors:
    BLACK = 'BLACK'
    RED = 'RED'
    GREEN = 'GREEN'
    YELLOW = 'YELLOW'
    BLUE = 'BLUE'
    MAGENTA = 'MAGENTA'
    CYAN = 'CYAN'
    WHITE = 'WHITE'


class Styles:
    BOLD = 'BOLD'
    UNDERLINE = 'UNDERLINE'
    LEVEL = 'LEVEL'
