import re

UNITS = {
    '': (0, 1),
    'b': (0, 1),
    'byte': (0, 1),
    'bytes': (0, 1),

    'k': (1, 1024),
    'm': (2, 1024),
    'g': (3, 1024),
    't': (4, 1024),
    'p': (5, 1024),
    'e': (6, 1024),
    'z': (7, 1024),
    'y': (8, 1024),

    'kb': (1, 1024),
    'mb': (2, 1024),
    'gb': (3, 1024),
    'tb': (4, 1024),
    'pb': (5, 1024),
    'eb': (6, 1024),
    'zb': (7, 1024),
    'yb': (8, 1024),

    'kib': (1, 1024),
    'mib': (2, 1024),
    'gib': (3, 1024),
    'tib': (4, 1024),
    'pib': (5, 1024),
    'eib': (6, 1024),
    'zib': (7, 1024),
    'yib': (8, 1024),

    'kilobyte': (1, 1024),
    'megabyte': (2, 1024),
    'gigabyte': (3, 1024),
    'terabyte': (4, 1024),
    'petabyte': (5, 1024),
    'exabyte': (6, 1024),
    'zettabyte': (7, 1024),
    'yottabyte': (8, 1024),

    'kilobytes': (1, 1024),
    'megabytes': (2, 1024),
    'gigabytes': (3, 1024),
    'terabytes': (4, 1024),
    'petabytes': (5, 1024),
    'exabytes': (6, 1024),
    'zettabytes': (7, 1024),
    'yottabytes': (8, 1024),

    'kibibyte': (1, 1024),
    'mebibyte': (2, 1024),
    'gibibyte': (3, 1024),
    'tebibyte': (4, 1024),
    'pebibyte': (5, 1024),
    'exbibyte': (6, 1024),
    'zebibyte': (7, 1024),
    'yobibyte': (8, 1024),

    'kibibytes': (1, 1024),
    'mebibytes': (2, 1024),
    'gibibytes': (3, 1024),
    'tebibytes': (4, 1024),
    'pebibytes': (5, 1024),
    'exbibytes': (6, 1024),
    'zebibytes': (7, 1024),
    'yobibytes': (8, 1024),
}


def parse_human_readable_size(value, base=10):
    if isinstance(value, str):
        matches = re.match(r'^(.*\d)\s*([a-zA-Z]*)$', value)

        if not matches:
            raise ValueError(
                'Invalid value "{}", passed for size parsing'.format(value))

        size_str, unit_str = matches.groups()
        size = int(size_str, base)

        value_unit = unit_str.lower()

        if value_unit not in UNITS:
            raise ValueError(
                'Invalid unit "{}", passed for size parsing.'.format(value_unit))

        exponent, unit_size = UNITS[value_unit]

        size *= unit_size ** exponent
        return size
    elif isinstance(value, int):
        return value
    else:
        raise ValueError('Invalid data type. Only "integer" and "string" allowed')
