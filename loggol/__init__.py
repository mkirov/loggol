import inspect
import hashlib
import atexit
import logging.config
import logging.handlers

from loggol.logger import Logger
from loggol.exceptions import LoggerError
from loggol import output
from loggol.levels import Levels
from loggol.constants import Colors
from loggol.level_styles import LevelStyles
from loggol.constants import Styles
from loggol.constants import Common


__all__ = ["get_logger", "class_logger", "setup", "add_level", "ClassLogWrapper"]

_LOGGERS = {}
_DEFAULT_WRAPPER_ATTRIBUTE = 'logger'

logging.setLoggerClass(Logger)


class ClassLogWrapper:

    _cached_ids = dict()
    _trace_separator = '->'

    def __init__(self, name, klass):
        self._klass = klass
        self._name = name

    def __get_subclasses(self, top_class=None, found_subclasses=None):

        if top_class is None:
            top_class = self._klass

        if found_subclasses is None:
            found_subclasses = list()
            found_subclasses.append(top_class)

        cls_subclasses = top_class.__subclasses__()

        if cls_subclasses:
            for cls_subclass in cls_subclasses:
                found_subclasses.append(cls_subclass)
                return self.__get_subclasses(cls_subclass, found_subclasses)

        return found_subclasses

    def __get_class_trace(self, source_func):

        source_func_id = ClassLogWrapper.__generate_func_id(source_func)

        if source_func_id in self._cached_ids:
            return self._cached_ids[source_func_id]
        else:
            subclasses = self.__get_subclasses()

            for subclass in subclasses:
                sub_class_mro = subclass.__mro__[1:-1]
                if source_func.co_name in subclass.__dict__:
                    sub_class_func_id = ClassLogWrapper.__generate_func_id(
                        subclass.__dict__[source_func.co_name].__code__)
                    if sub_class_func_id == source_func_id:
                        if sub_class_mro:
                            tt = [sc.__qualname__ for sc in reversed(sub_class_mro)]
                            tt.append(subclass.__qualname__)
                            class_trace = self._trace_separator.join(tt)
                        else:
                            class_trace = subclass.__qualname__

                        self._cached_ids[source_func_id] = class_trace
                        return class_trace
        return None

    @staticmethod
    def __generate_func_id(func):
        md5_constructor = hashlib.md5()
        md5_constructor.update(func.co_name.encode())
        md5_constructor.update(func.co_code)
        md5_constructor.update('_'.join(func.co_varnames).encode())
        md5_constructor.update(str(func.co_firstlineno).encode())

        return md5_constructor.hexdigest()

    def __getattr__(self, attr):

        stack = inspect.stack()
        full_func_stack = list()

        for s in stack[1:]:
            full_func_stack.append(s[0].f_code.co_name)
            path_trace = self.__get_class_trace(s[0].f_code)

            if path_trace is not None:
                break

        if path_trace is None:
            raise Exception(
                'Could not trace the path')

        path_trace = '.'.join(
            (path_trace, '.'.join(
                [
                    ''.join((s, '()'))
                    for s in reversed(full_func_stack)])))

        try:
            ret_logger = self.__getattribute__('-'.join((self._name, path_trace)))
        except AttributeError:
            ret_logger = get_logger(
                self._name,
                ''.join((Common.CLASS_SEPARATOR, path_trace)))

            setattr(self, '-'.join((self._name, path_trace)), ret_logger)

        return getattr(ret_logger, attr)


def _parse_children(name):
    """ Parses the name to extract it's
        children if available

        :param name: logger name for parsing
        :type name: str
        :return: base name children names
        :rtype tuple(base_name, children_names)
    """

    base_name = None
    children_names = None
    shred_logger_name = name.split('.')

    if len(shred_logger_name) > 1:
        base_name = shred_logger_name[0]
        children_names = shred_logger_name[1:]

    return base_name, children_names


def class_logger(name, attr_name=None):
    """ Decorator for class logging.
        All subclasses of a decorated class can
        reuse the specified attribute for logging.

        :param name: Name of a defined logger to
            be used in the class for logging.
        :type name: str
        :param attr_name: Attribute name to be used
            in the class for logging. Default is "logger"
        :type attr_name: str

    """
    if attr_name is None:
        attr_name = _DEFAULT_WRAPPER_ATTRIBUTE

    def class_wrapper(cls):

        if hasattr(cls, attr_name) \
                and isinstance(
                    getattr(cls, attr_name),
                    ClassLogWrapper):
            return cls

        cls_wrapper = ClassLogWrapper(name, cls)

        setattr(cls, attr_name, cls_wrapper)

        return cls

    return class_wrapper


def get_logger(name, child=None, *, children=None):
    """ Retrieves logger by name or it's child/children

        :param name: Name for the logger.
        :type name: str
        :param child: Child of the logger.
        :type child: str
        :param children: Children of the logger.
        :type children: list
        :raises LoggerIsMissingError: will raise
            if logger is missing from configured loggers
        :return: returns Logger instance
        :rtype: :class:`Logger`
    """

    children_loggers = list()
    parsed_base_name, parsed_children_names = _parse_children(name)

    if parsed_base_name is not None:
        name = parsed_base_name

    lgr = _LOGGERS.get(name)

    if not lgr:
        raise LoggerError(
            'Logger with name: "{}" is missing. '
            'Available loggers: {}'.format(
                name, [lgr for lgr in _LOGGERS]))

    if parsed_children_names is not None:
        children_loggers.extend(parsed_children_names)

    if child is not None:
        children_loggers.append(child)

    if children is not None:
        children_loggers.extend(children)

    if children_loggers:
        coupled_children = '.'.join(children_loggers)
        return lgr.getChild(coupled_children)

    return lgr


def setup(name=None, level=None, outputs=None):
    """ Setups the logger. Adds level and outputs
        and saves the configured logger for later
        usage.

        :param name: Name of the logger.
        :type name: string
        :param level: Logger level. All outputs
            without levels will inherit this one
            instead.
        :type level: string | int
        :param outputs: Outputs for the logger.
        :type outputs: list
        :raises LoggerAlreadyExsistsError: logger has
            already been configured.
    """

    if level is None:
        level = Levels.DEBUG

    if outputs is not None and not isinstance(outputs, list):
        outputs = [outputs]

    if outputs is None:
        outputs = list()

    parsed_base_name, parsed_children_names = _parse_children(name)

    if name in _LOGGERS:
        raise LoggerError(
            'Logger with name: "{}" already configured'.format(
                name))

    if parsed_base_name in _LOGGERS:
        raise LoggerError(
            'Logger with name "{base_logger_name}" already configured. '
            'Use get_logger method to get the children "{children_logger_name}" '
            'of the already configured logger "{base_logger_name}"'.format(
                base_logger_name=parsed_base_name,
                children_logger_name=parsed_children_names))

    lggr = logging.getLogger(name)
    lggr.setLevel(level)

    if outputs:
        lggr.add_outputs(outputs)
    else:
        lggr.default_output = output.StreamOutput()

    _LOGGERS[name] = lggr


def add_level(lvl_name, lvl_no, lvl_style=None):
    """ Adds level which can later be used in loggers.

        :param lvl_name: Name of the level.
        :type lvl_name: str
        :param lvl_no: No of the level for
          the level hierarchy.
        :type lvl_no: int
        :param lvl_style: Style of the level.
            Must be specified in a tuple format:

            (<HIGHLIGHT COLOR>, <FONT_COLOR>, <STYLE>)

        :type lvl_style: tuple
    """

    def log_msg(self, message, *args, **kwargs):
        if self.isEnabledFor(level_number):
            self._log(level_number, message, args, **kwargs)

    level_number = int(lvl_no)
    level_name_cap = lvl_name.upper()
    method_name = lvl_name.lower()

    logging.addLevelName(level_number, level_name_cap)

    Levels.add_level(level_name_cap, level_number)
    setattr(Logger, method_name, log_msg)

    if not lvl_style:
        lvl_style = (None, None, None)

    LevelStyles.add_level_style(level_name_cap, lvl_style)


def _stop():
    for lgr in _LOGGERS.values():
        lgr.remove_outputs(lgr.outputs)


atexit.register(_stop)
