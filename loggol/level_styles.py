from loggol.style import Font, Highlight, Style
from loggol.constants import Colors
from loggol.constants import Styles
from loggol.exceptions import LevelError
from loggol.levels import Levels


__all__ = ['LevelStyles']


class LevelStylesModel:

    _map = {
        'DEBUG': (None, Colors.CYAN, None),
        'INFO': (None, Colors.BLUE, None),
        'WARNING': (None, Colors.YELLOW, Styles.BOLD),
        'ERROR': (None, Colors.RED, Styles.BOLD),
        'CRITICAL': (Colors.RED, Colors.WHITE, Styles.BOLD)}

    def get_level_style(self, level):

        if level not in self._map:
            raise LevelError('Style for level {level} does not exist')

        highlight_color, font_color, style = self._map[level]

        style_items = list()

        if highlight_color:
            style_items.append(getattr(Highlight, highlight_color))

        if font_color:
            style_items.append(getattr(Font, font_color))

        if style:
            style_items.append(getattr(Style, style))

        return ''.join(style_items)

    def change_level_style(self, level, level_style):
        if isinstance(level, int):
            level = Levels.get_level_name(level)

        if level not in self._map:
            raise LevelError('Style for level {level} does not exist')

        self._map[level] = level_style

    def add_level_style(self, lvl_name, lvl_style):
        self._map[lvl_name] = lvl_style


LevelStyles = LevelStylesModel()
