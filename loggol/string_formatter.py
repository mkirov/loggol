from string import Formatter as StringFormatter
from loggol.level_styles import LevelStyles
from loggol.style import Font, Highlight, Style
from loggol.constants import Styles


__all__ = ['LoggolStringFormatter']


class LoggolStringFormatter(StringFormatter):

    _KEY_SPLIT_SYMBOL = '='
    _DEFAULT_LEVEL_COLORIZE_ITEMS = ('exception', 'level')

    def __init__(self, is_tty, colorized):
        self._is_tty = is_tty
        self._colorized = colorized

        self.styles_choices = dict(
            Font=Font,
            Highlight=Highlight,
            Style=Style)

        super().__init__()

    def get_value(self, key, args, kwargs):

        format_key_pieces = key.split(self._KEY_SPLIT_SYMBOL)
        if len(format_key_pieces) == 2:
            format_attribute = self.styles_choices.get(format_key_pieces[0])
            format_value = format_key_pieces[1]
            if format_attribute and format_value and self._is_tty and self._colorized:
                if format_value == Styles.LEVEL:
                    return LevelStyles.get_level_style(kwargs['level'])

                return getattr(format_attribute, format_value, '')
            else:
                return ''

        format_item = kwargs.get(key, '')
        if format_item \
                and key in self._DEFAULT_LEVEL_COLORIZE_ITEMS \
                and self._is_tty \
                and self._colorized:

            return ''.join(
                (LevelStyles.get_level_style(kwargs['level']),
                 format_item,
                 Style.CLEAR))

        return format_item
