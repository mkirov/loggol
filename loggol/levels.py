from loggol.exceptions import LevelError


__all__ = ['Levels']


class LevelsModel:

    _name_map = {
        'DEBUG': 10,
        'INFO': 20,
        'WARNING': 30,
        'ERROR': 40,
        'CRITICAL': 50
    }

    _num_map = {
        10: 'DEBUG',
        20: 'INFO',
        30: 'WARNING',
        40: 'ERROR',
        50: 'CRITICAL'
    }

    def add_level(self, lvl_name, lvl_no):

        if lvl_name in self._name_map:
            raise LevelError(
                'Level "{lvl}" already exists'.format(
                    lvl=lvl_name))

        if lvl_no in self._num_map:
            raise LevelError(
                'Duplicated level number {lvl_no} for "{lvl_new}", '
                'already taken by "{lvl_old}"'.format(
                    lvl_no=lvl_no,
                    lvl_new=lvl_name,
                    lvl_old=self._num_map[lvl_no]))

        self._name_map[lvl_name] = lvl_no
        self._num_map[lvl_no] = lvl_name

    def get_level_name(self, lvl_no):
        if lvl_no in self._num_map:
            return self._num_map[lvl_no]

        return None

    def get_level_num(self, lvl_name):
        if lvl_name in self._name_map:
            return self._name_map[lvl_name]

        return None

    def __getattr__(self, item):

        if item in self._name_map:
            return self._name_map[item]

        raise LevelError(
            'Invalid level "{lvl}".'.format(
                lvl=item))


Levels = LevelsModel()
