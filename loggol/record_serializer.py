from json import JSONEncoder
from datetime import datetime


class RecordSerializer(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            serial = obj.isoformat()
            return serial

        return obj.__dict__
