import unittest

import loggol


class TestAddLevel(unittest.TestCase):

    def test_basic_logger_setup(self):
        """ Test adding new level """

        level_name = 'CUSTOM'
        level_num = 15
        level_style = (None, loggol.Colors.BLUE, None)

        loggol.add_level(level_name, level_num, level_style)

        self.assertTrue(level_name in loggol.Levels._name_map and level_num in loggol.Levels._num_map)
        self.assertTrue(level_name in loggol.LevelStyles._map and loggol.LevelStyles._map[level_name] == level_style)


if __name__ == '__main__':
    unittest.main()