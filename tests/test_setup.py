import unittest
import shutil
import tempfile

import loggol
from loggol.logger import Logger
from loggol.exceptions import LoggerError


class TestSetup(unittest.TestCase):

    def setUp(self):
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    def test_basic_logger_setup(self):
        """ Test basic logger setup """

        loggol.setup('test_log_1')
        logger = loggol.get_logger('test_log_1')

        self.assertIsInstance(logger, Logger)
        self.assertEqual(logger.name, 'test_log_1')

    def test_existing_logger_setup(self):
        """ Tests for error throw when trying to
            setup logger with the same name
        """

        with self.assertRaises(LoggerError):
            loggol.setup('test_log_1')

    def test_existing_logger_setup(self):
        """ Tests for error throw when trying to
            setup logger with the same name but also
            add child
        """

        with self.assertRaises(LoggerError):
            loggol.setup('test_log_1.Child')

    def test_child_logger(self):
        """
            Test child logger
        """

        logger = loggol.get_logger('test_log_1.Child')
        self.assertEqual(logger.name, 'test_log_1.Child')

    def test_logger_outputs_setup(self):

        loggol.setup(
            'test_log_2',
            outputs=[
                loggol.output.BasicFileOutput(filename='test_file', directory=self.test_dir, level=loggol.Levels.DEBUG),
                loggol.output.StreamOutput(level=loggol.Levels.INFO)])

        logger = loggol.get_logger('test_log_2')

        self.assertTrue(len(logger.outputs) == 2)
        self.assertTrue(any([isinstance(x, loggol.output.BasicFileOutput) for x in logger.outputs]))
        self.assertTrue(any([isinstance(x, loggol.output.StreamOutput) for x in logger.outputs]))


if __name__ == '__main__':
    unittest.main()