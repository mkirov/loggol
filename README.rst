
.. image:: ./docs/source/_static/logo.png

Description
###########

Loggol idea is to make logging setup and usage straightforward.


- Straightforward setup/remove/add of outputs of the logger.
- Uses overridden logging handlers called now outputs.
- Supports colorful terminal output (the rainbow terminal).
- Supports customizing the color for each item in the format regarding colorful terminal.
- Supports JSON output.
- Supports QueueOutput for removing the waiting interval for time consuming logging.
- Supports adding/changing of custom levels and respectively their custom color, name and level hierarchy.
- Supports passing human readable sizes to a SizeRotatingFileOutput, which is an enhanced RotatingFileHandler.
- Supports concurrent multiprocessing and multithreading safe write for SizeRotatingFileOutput.
- Supports cron-like schedule for CronRotatingFileOutput, which is an enhanced TimedRotatingFileHandler.
- Supports compressing to zip rotated files by CronRotatingFileOutput which reduces size.
- Supports SQSOutput for sending log messages in JSON format.
- Supports HTTPOutput for sending post requests with JSON format to an API or other external service.
- Supports decorator for classes for easier integration and usage.


Examples
########


Define simple console logger
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    import loggol
    loggol.setup(name='somename', level=loggol.Levels.ERROR)


Get the defined logger:
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    logger = loggol.get_logger('somename')

Start using it

.. code-block:: python

    logger.error('Test error message')


Note:
- By default logger will have only one output (Stream) assigned with default colorization for terminal which support it.
- Level defined in setup method have a role as global for all outputs.

Defining multiple outputs
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    import loggol

    loggol.setup(
        name='somename',
        outputs=[
            loggol.output.StreamOutput(level=loggol.Levels.INFO),
            loggol.output.BasicFileOutput(
                filename="basic_file_log",
                directory='/example/directory',
                level=loggol.Levels.ERROR)])


Get the defined logger by name

.. code-block:: python

    logger = loggol.get_logger('somename')


Start using it

.. code-block:: python

    logger.info('Test info message')
    logger.error('Test error message')


Note:
- level defined in output is only for the respective output, but is not
overriding the logger level, so if output level is lower in hierarchy then
it will not be able to log.
- Default suffix for filename is File type outputs is 'log'. To change
need to pass suffix parameter.


Add output/s and remove output/s
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is possible also to add outputs after setup.

.. code-block:: python

    loggol.setup('somename')
    logger = loggol.get_logger('somename')

    rotatefile_output = logger.add_output(
        loggol.output.SizeRotatingFileOutput(
            'rotation_file_example',
            size="5MB"
            level=loggol.Levels.INFO))

    newly_added_outputs = logger.add_outputs([
        loggol.output.StreamOutput(level=loggol.Levels.ERROR),
        loggol.output.BasicFileOutput('example_dir/some_file_name', level=loggol.Levels.INFO)])


To see all outputs assigned to a logger

.. code-block:: python

    >>> logger.outputs
    >>> [<loggol.output.SizeRotatingFileOutput object at 0x7fd747c910f0>, <loggol.output.StreamOutput object at 0x7fd74758e160>, <loggol.output.BasicFileOutput object at 0x7fd74758e240>]


Now say we want to remove SizeRotatingFileOutput

.. code-block:: python

    logger.remove_output(rotatefile_output)


Also can use outputs to select an exact output/s if used setup to initialize the outputs

.. code-block:: python

    logger.remove_output(logger.outputs[0])


Remove multiple outputs:

.. code-block:: python

    logger.remove_outputs(newly_added_outputs)

or get certain type of outputs:

.. code-block:: python

    retrieved_outputs = logger.get_outputs(loggol.output.SizeRotatingFileOutput)
    logger.remove_outputs(retrieved_outputs)

or just remove all outputs:

.. code-block:: python

    logger.remove_outputs(logger.outputs)


Example of CronRotatingOutput
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    loggol.setup(
        'somename',
        level=loggol.Levels.DEBUG,
        outputs=loggol.output.CronRotatingFileOutput(
            filename='example_cron_file', backup=1, cron='*/1 * * * *'))

    lg = loggol.get_logger('somename')

    lg.info('Some info message')
    lg.error('Some error message')

Note:
    Can also use "compressed" parameter for zipping the files
    after rotating them to reduce their total size

Example of SizeRotatingOutput
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    loggol.setup(
        'somename',
        level=loggol.Levels.DEBUG,
        outputs=loggol.output.SizeRotatingFileOutput(
            'rotation_file_example',
            size="5MB"
            level=loggol.Levels.INFO))

    lg = loggol.get_logger('somename')

    lg.info('Some info message')
    lg.error('Some error message')

Note:
    Can also use "concurrent" parameter for multiprocessing/multithreading safe writes to the file

Example of JSON output
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

        loggol.setup(
        'somename',
        level=loggol.Levels.DEBUG,
        outputs=loggol.output.StreamOutput(
            level=loggol.Levels.INFO, serialize=True))


which output is:

.. code-block:: json

    {
        "time": "2019-01-01T00:00:00.111111",
        "name": "somename",
        "lineno": 47,
        "msg": "Some info message",
        "filename": "example.py",
        "module_name": "example",
        "level": "INFO",
        "levelno": 20,
        "thread": 140088107624256,
        "thread_name": "MainThread",
        "process": 20080,
        "process_name": "MainProcess",
        "function_name": "main()",
        "class_trace": null,
        "trace_path": "example",
        "pathname": "example.py",
        "exception": null
    }

Note:
    "serialized" parameter can be used on several of the outputs:
        - "StreamOutput"
        - "BasicFileOutput"
        - "CronRotatingFileOutput"
        - "SizeRotatingFileOutput"

Custom formatting
^^^^^^^^^^^^^^^^^

Custom formatting in loggol is using string format type. List of Styles and items to be used can be found below.
You can also set default format.

.. code-block:: python

    loggol.setup(
        'somename',
        outputs=loggol.output.StreamOutput(
            level=loggol.Levels.INFO,
            format="{Font=BLACK}[{time:%Y-%m-%d}]{Style=CLEAR} "
                   "{Font=BLUE}{Style=UNDERLINE}Thead: {thread}{Style=CLEAR}, "
                   "{Highlight=YELLOW}{Font=WHITE}{Style=BOLD}Process: {process} - {msg}{Style=CLEAR}"))

Note:
    You need to use Style=Clear if you want to cut the font/highlight/style to a certain point
    or it will cover the whole record.


Table of format styles:

+------------+------------+-----------+
| Font       | Highlight  | Style     |
+============+============+===========+
| BLACK      | BLACK      | CLEAR     |
+------------+------------+-----------+
| RED        | RED        | BOLD      |
+------------+------------+-----------+
| GREEN      | GREEN      | UNDERLINE |
+------------+------------+-----------+
| YELLOW     | YELLOW     |           |
+------------+------------+-----------+
| BLUE       | BLUE       |           |
+------------+------------+-----------+
| MAGENTA    | MAGENTA    |           |
+------------+------------+-----------+
| CYAN       | CYAN       |           |
+------------+------------+-----------+
| WHITE      | WHITE      |           |
+------------+------------+-----------+


Table of items:

+-----------------+----------------------------------------------------------------------------------------+
| Item            | Description                                                                            |
+=================+========================================================================================+
| time            | Time of the log message, need to specify it in datetime string (for example: %Y-%m-%d) |
+-----------------+----------------------------------------------------------------------------------------+
| name            | Name of the logger                                                                     |
+-----------------+----------------------------------------------------------------------------------------+
| lineno          | Line number                                                                            |
+-----------------+----------------------------------------------------------------------------------------+
| msg             | Message                                                                                |
+-----------------+----------------------------------------------------------------------------------------+
| filename        | Name of the file                                                                       |
+-----------------+----------------------------------------------------------------------------------------+
| module_name     | Name of the module                                                                     |
+-----------------+----------------------------------------------------------------------------------------+
| level           | Level name                                                                             |
+-----------------+----------------------------------------------------------------------------------------+
| levelno         | Level Number                                                                           |
+-----------------+----------------------------------------------------------------------------------------+
| thread          | Thread number                                                                          |
+-----------------+----------------------------------------------------------------------------------------+
| thread_name     | Thread name                                                                            |
+-----------------+----------------------------------------------------------------------------------------+
| process         | Process number (PID)                                                                   |
+-----------------+----------------------------------------------------------------------------------------+
| process_name    | Process name                                                                           |
+-----------------+----------------------------------------------------------------------------------------+
| function_name   | Function name                                                                          |
+-----------------+----------------------------------------------------------------------------------------+
| class_trace     | Classes trace chain                                                                    |
+-----------------+----------------------------------------------------------------------------------------+
| trace_path      | Full trace name including module name, classes chain and function name                 |
+-----------------+----------------------------------------------------------------------------------------+
| pathname        | Path name                                                                              |
+-----------------+----------------------------------------------------------------------------------------+


Setting default format
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    loggol.Common.DEFAULT_FORMAT = '{Font=BLACK}[{time:%Y-%m-%d}]{Style=CLEAR} {level} {msg}'


Add new level
^^^^^^^^^^^^^

.. code-block:: python

    loggol.add_level('CUSTOM', 25, (None, loggol.Colors.BLUE, loggol.Styles.UNDERLINE))  # (<HIGHLIGHT COLOR>, <FONT_COLOR>, <STYLE>)

    # Can be accessed with loggol.Levels.CUSTOM too once defined.

    loggol.setup('somename', level=loggol.Levels.DEBUG)

    logger = loggol.get_logger('somename')
    logger.custom('This message is from the custom error')

Note:
    Mind which level number you set for your custom level, because if you set it less than the level
    you are setting as an border you might not see it.


Change level style
^^^^^^^^^^^^^^^^^^

.. code-block:: python

        loggol.LevelStyles.change_level_style(
            loggol.Levels.INFO,    # can also use string like "INFO" instead
            (None, loggol.Colors.MAGENTA, None))  # (<HIGHLIGHT COLOR>, <FONT_COLOR>, <STYLE>)


Use QueueOutput to avoid time consuming logging slowing down
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    loggol.setup(
        'somename',
        outputs=loggol.output.QueueOutput([
            loggol.output.BasicFileOutput('example_file', level=loggol.Levels.DEBUG),
            loggol.output.EmailOutput(
                mail_host='localhost', from_address='from@some.com',
                to_address='to@some.com', subject='Sending error message',
                level=loggol.Levels.ERROR)]))

    lg = loggol.get_logger('somename')

    lg.info('Some info message')
    lg.error('Some error message')

Note:
    Above will pass every log message to a queue where listener
    running on a separate thread will take care of it and thus way it will not
    wait for it to finish especially for sending an email which could sometimes
    be slow.

Use loggol in your classes almost effortless
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    @loggol.class_logger('somename')
    class TestClass:

        def __init__(self, a):
            self.a = a
            self.logger.info('Checking Init ...')

        def test_func(self, a):
            c = a + 5
            self.logger.debug(f'Function returns {c}')

            return c

    if __name__ == "__main__":

        loggol.setup(
            'somename',
            outputs=[
                loggol.output.StreamOutput(level=loggol.Levels.DEBUG),
                loggol.output.BasicFileOutput(filename='example_file', level=loggol.Levels.WARNING)])

        test = TestClass(10)
        test.test_func(5)


Note:
- Default attribute for logger to use without specifying in decorator is "logger".
To change pass second parameter with the attribute name.

If you don't want to use the decorator, you can do:

.. code-block:: python

    import loggol

    class TestClass:

        def __init__(self, a):
            self.custom_logger = loggol.ClassLogWrapper('somename', TestClass)  # or self.__class__ instead of TestClass
            self.a = a
            self.custom_logger.info('Checking Init ...')

        def test_func(self, a):
            c = a + 5
            self.custom_logger.debug(f'Function returns {c}')

            return c

    if __name__ == "__main__":

        loggol.setup(
            'somename',
            outputs=[
                loggol.output.StreamOutput(level=loggol.Levels.DEBUG),
                loggol.output.BasicFileOutput('example_file', level=loggol.Levels.WARNING)])

        test = TestClass(10)
        test.test_func(5)



For questions and info please contact me via email: mariojkirov@gmail.com