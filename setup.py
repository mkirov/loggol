from setuptools import setup

setup(
    name='loggol',
    version='1.0.0',
    packages=['loggol'],
    url='',
    license='MIT license',
    author='mkirov',
    author_email='',
    description='loggol makes logging easier',
    install_requires=['croniter', 'portalocker', 'boto3', 'requests'],
    python_requires=">=3.4"
)
